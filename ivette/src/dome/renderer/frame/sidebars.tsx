/* ************************************************************************ */
/*                                                                          */
/*   This file is part of Frama-C.                                          */
/*                                                                          */
/*   Copyright (C) 2007-2021                                                */
/*     CEA (Commissariat à l'énergie atomique et aux énergies               */
/*          alternatives)                                                   */
/*                                                                          */
/*   you can redistribute it and/or modify it under the terms of the GNU    */
/*   Lesser General Public License as published by the Free Software        */
/*   Foundation, version 2.1.                                               */
/*                                                                          */
/*   It is distributed in the hope that it will be useful,                  */
/*   but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/*   GNU Lesser General Public License for more details.                    */
/*                                                                          */
/*   See the GNU Lesser General Public License version 2.1                  */
/*   for more details (enclosed in the file licenses/LGPLv2.1).             */
/*                                                                          */
/* ************************************************************************ */

// --------------------------------------------------------------------------
// --- SideBars
// --------------------------------------------------------------------------

/**
   @packageDocumentation
   @module dome/frame/sidebars
*/

import React from 'react';
import { useFlipSettings } from 'dome';
import { Badge } from 'dome/controls/icons';
import { Label } from 'dome/controls/labels';
import { classes } from 'dome/misc/utils';

import './style.css';

// --------------------------------------------------------------------------
// --- SideBar Container
// --------------------------------------------------------------------------

export interface SideBarProps {
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

/**
   Container for sidebar items.
 */
export function SideBar(props: SideBarProps) {
  const className = classes(
    'dome-xSideBar',
    'dome-color-frame',
    props.className,
  );
  return (
    <div className={className} style={props.style}>
      {props.children}
    </div>
  );
}

// --------------------------------------------------------------------------
// --- Badges Specifications
// --------------------------------------------------------------------------

export type BadgeElt = undefined | null | string | number | React.ReactNode;
export type Badges = BadgeElt | BadgeElt[];

const makeBadgeElt = (elt: BadgeElt): React.ReactNode => {
  if (elt === undefined || elt === null) return null;
  switch (typeof (elt)) {
    case 'number':
    case 'string':
      return <Badge value={elt} />;
    default:
      return elt;
  }
};

const makeBadge = (elt: Badges): React.ReactNode => {
  if (Array.isArray(elt))
    return elt.map(makeBadgeElt);
  return makeBadgeElt(elt);
};

// --------------------------------------------------------------------------
// --- SideBar Section Hide/Show Button
// --------------------------------------------------------------------------

const HideShow = (props: { onClick: () => void; visible: boolean }) => (
  <label
    className="dome-xSideBarSection-hideshow dome-text-label"
    onClick={props.onClick}
  >
    {props.visible ? 'Hide' : 'Show'}
  </label>
);

// --------------------------------------------------------------------------
// --- SideBar Section
// --------------------------------------------------------------------------

export interface SectionProps {
  /** Section label. */
  label: string;
  /** Section tooltip description. */
  title?: string;
  /** Hide/Show window settings. */
  settings?: string;
  /** Controlled Fold/Unfold state. */
  unfold?: boolean;
  /** Initial unfold state (default is `true`). */
  defaultUnfold?: boolean;
  /** Enabled sections are made visible. */
  enabled?: boolean;
  /** Disabled sections are made unvisible. */
  disabled?: boolean;
  /** Badge summary (only visible when folded). */
  summary?: Badges;
  /** Right-click callback. */
  onContextMenu?: () => void;
  /** Section contents. */
  children?: React.ReactNode;
}

/**
   Sidebar Section.

   Unless specified, sections can be hidden on click.
   When items in the section have badge(s)
   it is highly recommended to provide a badge summary to be displayed
   when the content is hidden.

   Sections with no items are not displayed.
*/
export function Section(props: SectionProps) {

  const [state, flipState] = useFlipSettings(
    props.settings,
    props.defaultUnfold,
  );

  const { enabled = true, disabled = false, children } = props;
  if (disabled || !enabled || React.Children.count(children) === 0)
    return null;
  const { unfold } = props;
  const foldable = unfold === undefined;
  const visible = unfold ?? state;
  const maxHeight = visible ? 'max-content' : 0;

  return (
    <div className="dome-xSideBarSection">
      <div
        className="dome-xSideBarSection-title dome-color-frame"
        title={props.title}
        onContextMenu={props.onContextMenu}
      >
        <Label label={props.label} />
        {!visible && makeBadge(props.summary)}
        {foldable && <HideShow visible={visible} onClick={flipState} />}
      </div>
      <div className="dome-xSideBarSection-content" style={{ maxHeight }}>
        {children}
      </div>
    </div>
  );
}

// --------------------------------------------------------------------------
// --- SideBar Items
// --------------------------------------------------------------------------

export interface ItemProps {
  /** Item icon. */
  icon?: string;
  /** Item label. */
  label?: string;
  /** Item tooltip text. */
  title?: string;
  /** Badge. */
  badge?: Badges;
  /** Enabled item. */
  enabled?: boolean;
  /** Disabled item (dimmed). */
  disabled?: boolean;
  /** Selection state. */
  selected?: boolean;
  /** Selection callback. */
  onSelection?: () => void;
  /** Right-click callback. */
  onContextMenu?: () => void;
  /** Additional class. */
  className?: string;
  /** Additional styles. */
  style?: React.CSSProperties;
  /** Other item elements. */
  children?: React.ReactNode;
}

/** Sidebar Items. */
export function Item(props: ItemProps) {
  const { selected = false, disabled = false, enabled = true } = props;
  const isDisabled = disabled || !enabled;
  const ref = React.useRef<HTMLDivElement>(null);
  const [clicked, setClicked] = React.useState(false);
  const onSelection = isDisabled ? undefined : props.onSelection;
  const onClick =
    onSelection ? () => { setClicked(true); onSelection(); } : undefined;
  const onContextMenu = isDisabled ? undefined : props.onContextMenu;
  const className = classes(
    'dome-xSideBarItem',
    selected ? 'dome-active' : 'dome-inactive',
    isDisabled && 'dome-disabled',
    props.className,
  );

  React.useLayoutEffect(() => {
    if (!clicked && selected) {
      ref?.current?.scrollIntoView({
        behavior: 'auto',
        block: 'center',
      });
    }
    if (!selected && clicked)
      setClicked(false);
  }, [clicked, selected]);

  return (
    <div
      ref={ref}
      className={className}
      style={props.style}
      title={props.title}
      onContextMenu={onContextMenu}
      onClick={onClick}
    >
      <Label icon={props.icon} label={props.label} />
      {props.children}
      {makeBadge(props.badge)}
    </div>
  );
}

// --------------------------------------------------------------------------
