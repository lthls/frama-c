/* ************************************************************************ */
/*                                                                          */
/*   This file is part of Frama-C.                                          */
/*                                                                          */
/*   Copyright (C) 2007-2021                                                */
/*     CEA (Commissariat à l'énergie atomique et aux énergies               */
/*          alternatives)                                                   */
/*                                                                          */
/*   you can redistribute it and/or modify it under the terms of the GNU    */
/*   Lesser General Public License as published by the Free Software        */
/*   Foundation, version 2.1.                                               */
/*                                                                          */
/*   It is distributed in the hope that it will be useful,                  */
/*   but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/*   GNU Lesser General Public License for more details.                    */
/*                                                                          */
/*   See the GNU Lesser General Public License version 2.1                  */
/*   for more details (enclosed in the file licenses/LGPLv2.1).             */
/*                                                                          */
/* ************************************************************************ */

// --------------------------------------------------------------------------
// --- Eva Values
// --------------------------------------------------------------------------

// External Libs
import { throttle } from 'lodash';
import equal from 'react-fast-compare';
import * as Dome from 'dome';

import * as Server from 'frama-c/server';
import * as States from 'frama-c/states';
import * as Values from 'frama-c/api/plugins/eva/values';

// Model
import { Probe } from './probes';
import { StacksCache, Callsite } from './stacks';
import { ModelCallbacks, ValueCache } from './cells';
import { LayoutProps, LayoutEngine, Row } from './layout';

export interface ModelLayout extends LayoutProps {
  location?: States.Location;
}

export type EvalStmt = 'Before' | 'After';
export type EvalCond = 'Cond' | 'Then' | 'Else';

/* --------------------------------------------------------------------------*/
/* --- EVA Values Model                                                   ---*/
/* --------------------------------------------------------------------------*/

export class Model implements ModelCallbacks {

  constructor() {
    this.forceUpdate = this.forceUpdate.bind(this);
    this.forceLayout = this.forceLayout.bind(this);
    this.forceReload = this.forceReload.bind(this);
    this.computeLayout = this.computeLayout.bind(this);
    this.setLayout = throttle(this.setLayout.bind(this), 300);
    this.metaSelection = this.metaSelection.bind(this);
    this.getRowKey = this.getRowKey.bind(this);
    this.getRowCount = this.getRowCount.bind(this);
    this.getRowLines = this.getRowLines.bind(this);
    this.isFolded = this.isFolded.bind(this);
  }

  // --- Probes

  private vstmt: EvalStmt = 'Before';
  private vcond: EvalCond = 'Cond';
  private selected?: Probe;
  private focused?: Probe;
  private callstack?: Values.callstack;
  private remanent?: Probe; // last transient
  private probes = new Map<string, Probe>();
  private folded = new Map<string, boolean>(); // folded functions
  private byCallstacks = new Map<string, boolean>();

  getFocused() { return this.focused; }
  isFocused(p: Probe | undefined) { return this.focused === p; }

  getProbe(location: States.Location | undefined): Probe | undefined {
    if (!location) return undefined;
    const { fct, marker } = location;
    if (fct && marker) {
      let p = this.probes.get(marker);
      if (!p) {
        p = new Probe(this, fct, marker);
        this.probes.set(marker, p);
        p.requestProbeInfo();
      }
      return p;
    }
    return undefined;
  }

  addProbe(location: States.Location) {
    const { fct, marker } = location;
    if (fct && marker) {
      const probe = new Probe(this, fct, marker);
      probe.setPersistent();
      probe.requestProbeInfo();
      this.probes.set(marker, probe);
      this.forceLayout();
    }
  }

  removeProbe(probe: Probe) {
    probe.setTransient();
    if (this.selected === probe)
      this.clearSelection();
  }

  getStacks(p: Probe | undefined): Values.callstack[] {
    return p ? this.stacks.getStacks(p.marker) : [];
  }

  getVstmt(): EvalStmt { return this.vstmt; }
  getVcond(): EvalCond { return this.vcond; }
  setVstmt(s: EvalStmt) { this.vstmt = s; this.forceUpdate(); }
  setVcond(s: EvalCond) { this.vcond = s; this.forceUpdate(); }

  isFolded(fct: string): boolean {
    return (this.focused?.fct !== fct) && (this.folded.get(fct) ?? false);
  }

  isFoldable(fct: string): boolean {
    return this.focused?.fct !== fct;
  }

  setFolded(fct: string, folded: boolean) {
    this.folded.set(fct, folded);
    this.forceLayout();
  }

  isByCallstacks(fct: string): boolean {
    return this.byCallstacks.get(fct) ?? false;
  }

  setByCallstacks(fct: string, b: boolean) {
    this.byCallstacks.set(fct, b);
    this.forceLayout();
  }

  clearFunction(fct: string) {
    let selected = false;
    this.probes.forEach((p) => {
      if (p.fct === fct) {
        p.setTransient();
        if (this.selected === p)
          selected = true;
      }
    });
    if (selected)
      this.clearSelection();
  }

  // --- Caches

  readonly stacks = new StacksCache(this);
  readonly values = new ValueCache(this);

  // --- Rows

  private lock = false;
  private layout: ModelLayout = { margin: 80 };
  private rows: Row[] = [];

  getRow(index: number): Row | undefined {
    return this.rows[index];
  }

  getRowCount() {
    return this.rows.length;
  }

  getRowKey(index: number): string {
    const row = this.rows[index];
    return row ? row.key : `#${index}`;
  }

  getRowLines(index: number): number {
    const row = this.rows[index];
    return row ? row.hlines : 0;
  }

  setSelectedRow(row: Row) {
    const cs = row.callstack;
    if (cs !== this.callstack) {
      this.callstack = cs;
      this.forceUpdate();
    }
  }

  isSelectedRow(row: Row): boolean {
    if (!this.byCallstacks) return false;
    const cs = this.callstack;
    return cs !== undefined && cs === row.callstack;
  }

  isAlignedRow(row: Row): boolean {
    const cs = this.callstack;
    const cr = row.callstack;
    return cs !== undefined && cr !== undefined && this.stacks.aligned(cs, cr);
  }

  getCallstack(): Values.callstack | undefined {
    return this.callstack;
  }

  getCalls(): Callsite[] {
    const c = this.callstack;
    return c === undefined ? [] : this.stacks.getCalls(c);
  }

  // --- Throttled
  setLayout(ly: ModelLayout) {
    if (!equal(this.layout, ly)) {
      this.layout = ly;
      this.selected = this.getProbe(ly.location);
      this.forceLayout();
    }
  }

  metaSelection(location: States.Location) {
    const p = this.getProbe(location);
    if (p && p.transient)
      p.setPersistent();
  }

  clearSelection() {
    this.focused = undefined;
    this.selected = undefined;
    this.remanent = undefined;
    this.forceLayout();
  }

  // --- Recompute Layout

  private computeLayout() {
    if (this.lock) return;
    this.lock = true;
    const s = this.selected;
    if (!s) {
      this.focused = undefined;
      this.remanent = undefined;
    } else if (!s.loading) {
      this.focused = s;
      if (s.code && s.transient) {
        this.remanent = s;
      } else {
        this.remanent = undefined;
      }
    }
    const toLayout: Probe[] = [];
    this.probes.forEach((p) => {
      if (p.code && (!p.transient || p === this.remanent)) {
        toLayout.push(p);
      }
    });
    const engine = new LayoutEngine(
      this.layout,
      this.values,
      this.stacks,
      this.isFolded,
    );
    this.rows = engine.layout(toLayout, this.byCallstacks);
    this.laidout.emit();
    this.lock = false;
  }

  // --- Force Reload (empty caches)
  forceReload() {
    this.focused = undefined;
    this.remanent = undefined;
    this.selected = undefined;
    this.callstack = undefined;
    this.probes.clear();
    this.stacks.clear();
    this.values.clear();
    this.forceLayout();
    this.forceUpdate();
  }

  // --- Events
  readonly changed = new Dome.Event('eva-changed');
  readonly laidout = new Dome.Event('eva-laidout');

  // --- Force Layout
  forceLayout() {
    setImmediate(this.computeLayout);
  }

  // --- Foce Update
  forceUpdate() { this.changed.emit(); }

  // --- Global Signals

  mount() {
    States.MetaSelection.on(this.metaSelection);
    Server.onSignal(Values.changed, this.forceReload);
  }

  unmount() {
    States.MetaSelection.off(this.metaSelection);
    Server.offSignal(Values.changed, this.forceReload);
  }

}

export interface ModelProp {
  model: Model;
}
